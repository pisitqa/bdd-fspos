Feature: Owner

   Feature POS

   Scenario: Change Rounding Setting Rounding type
   Given  on Rounding Setting page
   When   I Click Rounding type
   Then   I Expect Display Rounding type Drop down list

   Scenario: Change Rounding Setting No Rounding
   Given  on Rounding Setting page
   When   I Click Rounding type
   And    I Select No Rounding Done
   Then   I Expect Display No Rounding

   Scenario: Change Rounding Setting Rounding Up
   Given  on Rounding Setting page
   When   I Click Rounding type
   And    I Select Rounding Up
   Then   I Expect Display Rounding Up

   Scenario: Change Rounding Setting Rounding Down
   Given  on Rounding Setting page
   When   I Click Rounding type
   And    I Select Rounding Down
   Then   I Expect Display Rounding Down

   Scenario: Change Rounding Setting Rounding Up & Rounding Down
   Given  on Rounding Setting type
   When   I Click Rounding type
   And    I Select Rounding Up & Rounding Down
   Then   I Expect Display Rounding Up & Rounding Down

   Scenario: Change Rounding amount Setting Rounding page
   Given  on Rounding Setting type
   When   I Click Rounding Amount Select 1.00
   Then   I Expect Display Rounding Up & Rounding Down 1.00