Feature: App Owner FoodStory

   Feature Description Printer Setting

   Scenario: Setup Name printer Setting Printer page
   Given  on Printer Setting page
   When   I Click setup name print
   And    I select Invoice & Receipt and Order Ticket
   Then   I Expect display drop list Invoice & Receipt,Order Ticket

   Scenario: Setup Name Invoice & Receipt Main Name,Receipt and Order Ticket Setting Printer page
   Given  on Printer Setting page
   When   I Click Setup Name print Main Name
   And    I Select Invoice & Receipt 'Main Name'
   And    I Select Receipt and Order Ticket 'Main Name'
   And    I click Done
   And    I go back Select table 7 select 2 people
   And    I Select Order Menu 'สเต็กเนื้อโคตรฟิน' Send Order
   Then   I Expect Print display Order Ticket Main Name 'สเต็กเนื้อโคตรฟิน'
   And    I Pay Order bill display all bill Main Name 'สเต็กเนื้อโคตรฟิน'
   And    I Expect Print Order Menu Main Name 'สเต็กเนื้อโคตรฟิน'

   Scenario: Setup Name Invoice & Receipt Name 2,Receipt and Order Ticket Setting Printer page
   Given  on Printer Setting page
   When   I Click Setup Name Print Name 2
   And    I Select Invoice & Receipt 'Name 2'
   And    I Select Receipt and Order Ticket 'Name 2'
   And    I Click Done
   And    I Back Order Menu
   And    I Click Table 6 fill 2 people Select Done
   And    I select Order Menu 'สเต็กหมู' Send Order
   Then   I Expect Print display Order Ticket Name 2 'หมูย่าง'
   And    I Pay Order bill display all bill Name2 'หมูย่าง'
   And    I Expect Print Order Menu Name2 'หมูย่าง'

   Scenario: Setup Name Receipt and Order Ticket Menu Code Setting printer page
   Given  on Printer Setting page
   When   I Click Setup Name print Menu Code 
   And    I Select Receipt and Order Ticket 'Menu Code'
   And    I Click Done
   And    I Back Order Menu
   And    I Click Table 5 fill 2 People Select Done
   And    I select Order Menu 'สเต็กหมู' Send Order
   Then   I Expect Print Order Menu Menu Code 'MU001'
   
   Scenario: Change Setup Printer Menu
   Given  on Printer Setting page
   When   I Click Setup Printer menu
   Then   I Expect display all Category Menu

   Scenario: Change Setup Printer Menu Select Category Printer Order
   Given  on Printer Setting page
   When   I Click Setup Printer Menu
   And    I Select Category 'สเต็ก'
   And    I Click select printer 'แคชเชียร์' Done
   And    I Go back Select Table
   And    I Click Table 5 fill 2 People Done
   And    I Select Order Menu 'สเต็กเนื้อโคตรฟิน'
   Then   I Expect display printer Menu 'สเต็กเนื้อโคตรฟิน'