Feature: Owner

   Feature Description Setting Inventory Pos

   Scenario: Change Custom Po Address Setting Inventory page
   Given  on Inventory Setting page
   When   I Click Function Custom PO Address
   Then   I Expect Display Open Custom PO Address

   Scenario: Change Fill Custom PO Address Setting Inventory page
   Given  on Inventory Setting page
   When   I Fill 'Company name'
   And    I Fill 'TAX ID'
   And    I Fill 'Address'
   Then   I Expect Display PO Address 

   Scenario: Change Document Running Number Setting Inventory page
   Given  on Inventory Setting page
   When   I Click UnLocked 
   And    I Fill Document Prefix
   And    I Select Running Type 'yyyymm'
   Then   I Expect Display Doument Running Number

   Scenario: Change Safety Stock Alert Setting Inventory page
   Given  on Inventory Setting page
   When   I Fill 'arnon.3342@gmail.com' ADD
   And    I Select Send time '17.30'
   Then   I Expect Safety Stock Alert Send Email