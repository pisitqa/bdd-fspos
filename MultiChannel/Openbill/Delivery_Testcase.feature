## Testcase ของ default กับ custom จะเหมือนกัน จะต่างกันตรงที่ Pop-up field ต่างกัน
Scenario: Display pop-up delivery default or custom channel 
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    Then I expect I see pop-up created bill delivery
    And I see header pop-up 'xxxx' channel

Scenario: Not enter the required field
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I tap save button
    Then I expect I see dialog error

Scenario: Input phone number thailand 9 digits
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I input phone number 'xxxxxxxxx'
    And I input data required already
    Then I expect I see dialog error

Scenario: Input phone number thailand 10 digits
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I input phone number 'xxxxxxxxxx'
    And I input data required already
    Then I expect correctly record data

Scenario: Search the phone number that used to be used
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I input phone number 'xxxxxxxxxx'
    Then I expect I see the phone number that used to be used

Scenario: Display pop-up search map 
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I tap map field
    Then I expect I see pop-up find address

Scenario: Search input address by map
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I tap map field
    And I input address 'xxxx'
    Then I expect I see address 'xxxx'
    And I tap address 'xxxx' 
    Then I expect I see address 'xxxx' in map field

Scenario: Search address by pin map
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I tap map field
    And I tap select via map button
    Then I expect I see map on Display
    And I scroll map pin
    Then I can scroll map pin
    And I tap confrim button
    Then I expect I see address 'xxxx' in map field

Scenario: Specify the address manually
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I tick address manually button
    And I input address 'xxxxx'
    And I input data required already
    Then I expect correctly record data


    




