## Testcase ของ default กับ custom จะเหมือนกัน จะต่างกันตรงที่ Pop-up field ต่างกัน
Scenario: Display pop-up Takeaway default or custom channel 
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    Then I expect I see pop-up created bill delivery
    And I see header pop-up 'xxxx' channel

Scenario: Not enter the required field
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I tap save button
    Then I expect I see dialog error

Scenario: Input phone number thailand 9 digits
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I input phone number 'xxxxxxxxx'
    And I input data required already
    Then I expect I see dialog error

Scenario: Input phone number thailand 10 digits
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I input phone number 'xxxxxxxxxx'
    And I input data required already
    Then I expect correctly record data

Scenario: Search the phone number that used to be used
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill button
    And I choose type delivery
    And I choose 'xxxxx' channel
    And I input phone number 'xxxxxxxxxx'
    Then I expect I see the phone number that used to be used

