Scenario: Type delivery Restaurant use SVC
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill delivery already
    And Send order already
    Then I expect I see SVC untick

Scenario: Type takeaway Restaurant use SVC
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill takeaway already
    And Send order already
    Then I expect I see SVC untick

## มี delivery fee กรณีที่สั่งมาจาก online order 
Scenario: Type delivery have delivery fee
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill delivery already
    And Send order already
    Then I expect I see delivery fee correct data

## ที่เป็น custom จะdelivery fee show 0 ทั้งหมด
Scenario: Type delivery no delivery fee
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill delivery already
    And Send order already
    Then I expect delivery fee show 0 bath

Scenario: Edit detail customer
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill delivery or takeaway already
    And Send order already
    Then I'm on payment page
    And I tap edit customer buttom
    Then I expect I see pop-up edit detail 
    And pop-up edit show original data

   


