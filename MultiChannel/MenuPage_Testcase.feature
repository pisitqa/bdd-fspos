Scenario: Display type default service type delivery menu page
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill delivery default already
    Then I'm on menu page
    And Header show /deliver
    And Show correct data

Scenario: Display type custom service type delivery on menu page
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill delivery custom already
    Then I'm on menu page
    And Header show /deliver custom-orderNumber 
    And Show correct data

Scenario: Display type default service type takeaway menu page
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill takeaway default already
    Then I'm on menu page
    And Header show /takeaway
    And Show correct data

Scenario: Display type custom service type takeaway on menu page
    Given I Launched An App 
    When Login and selected branch already
    And I'm on main page
    And I tap open bill takeaway custom already
    Then I'm on menu page
    And Header show /takeaway custom-orderNumber 
    And Show correct data




