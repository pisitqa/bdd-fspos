Feature: Discount

Scenario: Discount percent per item
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab percent per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Discount Price per item
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Price per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Discount Total percent
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Total percent
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Discount Total price
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Total price
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 


Scenario: Discount percent per item and total percent
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab percent per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Total percent
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Discount percent per item and total price
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab percent per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Total price
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Discount price per item and total percent
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab price per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Total percent
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Discount price per item and total price
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab price per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab Total price
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: discount on selected menu by percent per item
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered

    When  Tap and hold on selected menu
    Then  select discount
    And   Tap button percent per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: discount on selected menu by price per item
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered

    When  Tap and hold on selected menu
    Then  select discount
    And   Tap button price per item
    And   Input value 'xx' to be discount
    And   Tab button done
    Then  Input reason to discount
    And   Tab button ok
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 


Scenario: Error message when input out of limit
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    And   Tab button pay
    Then  I expect I saw the menu I ordered
    And   Tab discount
    Then  Input pin 'xxxx'
    And   Tab percent per item
    And   Input value more limit 
    Then  I see error message 
    And   Tap button ok

    When  Tap total percent
    And   Input value more limit 
    Then  I see error message 
    And   Tap button ok
    
    When  Tap price per item
    And   Input value more limit 
    And   Tap button ok
    Then  I see error message
    And   Tap button ok

    When  Tap Total price
    And   Input value more limit 
    And   Tap button ok
    Then  I see error message
    And   Tap button ok