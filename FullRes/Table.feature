Scenario: Open table
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select 2 guests 
    And   Back to main page
    Then  I expect table 'x' open

Scenario: Unseat table
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select 2 guests 
    And   Back to main page
    Then  I expect table 'x' open
    And   Select table 'x'
    And   Tab button Unseat
    And   Input pin
    Then  I expect table 'x' close 

Scenario: Change table
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select 2 guests 
    And   Back to main page
    Then  I expect table 'x' open
    And   Select table 'x'
    And   Tab button Change Table
    And   Select table 'x'
    Then  I expect see pop-up confrim change table 
    And   I expect table 'x' open

Scenario: Merge table
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open two table 
    And   Back to main page
    And   Select table 'x'
    And   Tab button Change Table
    And   Select table 'x'
    Then  I expect see pop-up confrim merge table 
    And   I see icon merge table 

