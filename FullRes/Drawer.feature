Scenario: Open drawer 
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    Then  I expect see pop-up drawer
    And   Input amount of cash in drawer 'xxxx'
    Then  I expect see pop-up confrim start drawer
    And   I expect see pop-up success 

Scenario: Not Open drawer
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    Then  I expect see pop-up drawer
    And   Tab button cancel on pop-up drawer
    Then  I expect I'm on main page
    And   Tab navigation menu 
    And   Tab button drawer
    Then  I expect see pop-up drawer

Scenario: Paid in on drawer page
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer 
    And   Tab navigation menu 
    And   Tab button drawer
    And   Input amount 'xxxx'
    And   Tab button paid in 
    Then  I expect see 'xxxx' on drawer page

Scenario: Paid out on drawer page
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer 
    And   Tab navigation menu 
    And   Tab button drawer
    And   Input amount 'xxxx'
    And   Tab button paid out
    Then  I expect see '-xxxx' on drawer page

Scenario: End drawer amount error 
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer 
    And   Tab navigation menu 
    And   Tab button drawer
    And   Tab button end drawer 
    Then  I expect see pop-up error 

Scenario: End drawer amount successfully
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer 
    And   Tab navigation menu 
    And   Tab button drawer
    And   Input amount 'xxxx'
    And   Tab button end drawer 
    And   Tab button confrim 
    Then  I expect see pop-up success 

Scenario: Tab button x report
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer 
    And   Tab navigation menu 
    And   Tab button drawer
    And   Tab button x report
    Then  I expect can tab button x report