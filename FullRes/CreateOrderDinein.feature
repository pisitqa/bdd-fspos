Scenario: Create Order from menu list
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send and pay from order list 
    Then  I expect I'm on pay page
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Create Order from Open menu
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x'
    And   Tab button open menu
    And   Create menu name 'xxx' and input amount 'xxx'
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send and pay from order list 
    Then  I expect I'm on pay page
    And   Tab for exact amount 
    And   Select payment type cash 
    Then  I expect I'm on payment receipt page
    When  I tab button finish on payment receipt page
    Then  I expect I'm on main page 

Scenario: Delete menu from menu list
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   I want to delete menu 'xxx' 
    Then  I expect I didn't see the menu from order list 

Scenario: Delete menu from Open menu
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x'
    And   Tab button open menu
    And   Create menu name 'xxx' and input amount 'xxx'
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   I want to delete menu 'xxx' 
    Then  I expect I didn't see the menu from order list 

Scenario: Cancel menu from menu list
     Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send from order list 
    And   Press menu 'x' 
    And   Tab button Cancel order 
    Then  I expect see pin page
    And   Input pin
    Then  I expect see message cancel and return stock

Scenario: Cancel menu from Open menu
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x'
    And   Tab button open menu
    And   Create menu name 'xxx' and input amount 'xxx'
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send from order list 
    And   Press menu 'x' 
    And   Tab button Cancel order 
    Then  I expect see pin page
    And   Input pin
    Then  I expect see message cancel and return stock

Scenario: Cancel menu from menu list
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send from order list 
    And   Press menu 'x' 
    And   Tab button Cancel order 
    Then  I expect see pin page
    And   Input pin
    Then  I expect see message cancel and return stock

Scenario: Duplicate menu from menu list
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x' 
    And   Select menu 'xxx' from menu list 
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send from order list 
    And   Press menu 'x' 
    And   Tab button Duplicate
    Then  I expect menu quantity 2 

Scenario: Cancel menu from Open menu
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    And   Open drawer
    And   I want to open table 'x'
    And   Tab button open menu
    And   Create menu name 'xxx' and input amount 'xxx'
    And   Tab button send order 
    Then  I expect I saw the menu I ordered
    And   Tab button send from order list 
    And   Press menu 'x' 
    And   Tab button Duplicate
    Then  I expect menu quantity 2 





