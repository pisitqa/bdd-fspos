Scenario: Check screen pin page
    Given I Launched an App
    When  Login successfully and select branch already
    Then  I expect I'm on pin page

Scenario: Invalid pin
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    Then  I expect see message Pin Incorrect

Scenario: Input pin correct
    Given I Launched an App
    When  Login successfully and select branch already
    And   Input pin 'xxxxx'
    Then  I expect I'm on main page

Scenario: Refresh on pin page
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button refresh on pin page
    Then  I expect I'm on Pin page

Scenario: Staff clock in Invalid pin
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button clock in on pin page
    And   Input pin 'xxxxx'
    Then  I expect see message Pin Incorrect

Scenario: Staff clock in Input pin correct
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button clock in on pin page
    And   Input pin 'xxxxx'
    Then  I expect see message clock in on pin page

Scenario: Staff clock out Invalid pin
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button clock out on pin page
    And   Input pin 'xxxxx'
    Then  I expect see message Pin Incorrect

Scenario: Staff clock out Input pin correct
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button clock out on pin page
    And   Input pin 'xxxxx'
    Then  I expect see message clock out on pin page

Scenario: Logout on pin page case cancel
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button Logout on pin page
    Then  I expect see pop-up confirm logout
    And   I tab button cancel
    Then  I expect I'm on Pin page

Scenario: Logout on pin page case ok
    Given I Launched an App
    When  Login successfully and select branch already
    And   Tab button Logout on pin page
    Then  I expect see pop-up confirm logout
    And   I tab button OK
    Then  I expect I'm on Login page


