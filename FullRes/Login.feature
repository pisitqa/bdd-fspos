Scenario: fields is empty
    Given I Launched an App
    And   the field 'email' is empty
    And   the field 'password' is empty
    And   I click on 'Login'
    Then  I expect I'm on login page

Scenario: field username is empty
    Given I Launched an App
    When  I type 'qa_demo@test.com' in 'email'
    And   the field 'password' is empty
    And   I click on 'Login'
    Then  I expect I'm on login page

Scenario: field password is empty
    Given I Launched an App
    When  the field 'email' is empty
    And   I type 'Password2020' in 'password'
    And   I click on 'Login'
    Then  I expect I'm on login page

 Scenario: Email invalid format
    Given I Launched an App
    When  I type 'uuuuu' in 'email'
    And  I type 'Password2020' in 'password'
    And  I click on 'enter'
    Then I should see 'Email invalid format'

 Scenario: Wrong username 
    Given I Launched an App
    When  I type 'uuuuu@gmail.com' in 'email'
    And  I type 'Password2020' in 'password'
    And  I click on 'enter'
    Then I should see 'Invalid user or password'

 Scenario: Wrong password
    Given I Launched an App
    When  I type 'qa_demo@test.com' in 'email'
    And  I type '123456' in 'password'
    And  I click on 'enter'
    Then I should see 'E-mail or password is incorrect'

Scenario: Login successfully
    Given I Launched an App
    When I type 'qa_demo@test.com' in 'email'
    And  I type 'Password2020' in 'password'
    And  I click on 'Login'
    Then I expect I'm on branch page


